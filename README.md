## full_oppo6762-user 10 QP1A.190711.020 bedd37e98646d3a1 release-keys
- Manufacturer: realme
- Platform: mt6765
- Codename: RMX1941
- Brand: realme
- Flavor: lineage_RMX1941-userdebug
- Release Version: 11
- Id: RQ3A.211001.001
- Incremental: eng.carn4g.20211015.025532
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: 480
- Fingerprint: google/redfin/redfin:11/RQ3A.210805.001.A1/7474174:user/release-keys
- OTA version: 
- Branch: full_oppo6762-user-10-QP1A.190711.020-bedd37e98646d3a1-release-keys
- Repo: realme_rmx1941_dump_5354


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
